import mongoose from 'mongoose';
const Schema = mongoose.Schema;

/*
 * const OBSERVADOR = 'OBSERVADOR';
 * const DELIVERY = 'DELIVERY';
 * const ADMIN = 'ADMIN';
 */
export const DEFAULT_TIPO_USUARIO = 'OBSERVADOR';
const TipoUsuarioSchema = new Schema({
  nombre: {
    type: String,
    required: true,
    unique: true
  },
  activo: {
    type: Boolean,
    default: true
  }
}, { timestamps: true });
export const TipoUsuario = mongoose.model('tipoUsuario', TipoUsuarioSchema);


// === USER ===
const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  tipo: {
    type: Schema.Types.ObjectId,
    ref: 'tipoUsuario'
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  activo: {
    type: Boolean,
    default: true
  }
}, { timestamps: true });
export default mongoose.model('user', UserSchema);
