import { require } from '../../require.js';

const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const config = require('config');
const jwt = require('jsonwebtoken');

// Middleware
import auth from '../../middleware/auth.js';

// User model
import User, { TipoUsuario, DEFAULT_TIPO_USUARIO } from '../../models/User.js';

// @route  GET api/users
// @desc   Get all users
// @access Public
router.get('/', auth, (req, res) => {
  //TODO: Only an admin can do it
  User.find()
    .select('-password')
    .sort({ date: -1 })
    .then(users => res.json(users))
});

// @route  POST api/users
// @desc   Create an user
// @access Public
router.post('/', async (req, res) => {
  const { name, email, password } = req.body;
  if (!name || !email || !password) {
    return res.status(400).json({ msg: 'Please enter all fields' });
  }

  // Check for existing users
  const user = await User.findOne({ email });
  if (user) {
    return res.status(400).json({ msg: 'User already exists' });
  }

  const tipo = await TipoUsuario.findOne({ nombre: DEFAULT_TIPO_USUARIO });
  if (!tipo) {
    return res.status(400).json({ msg: 'Tipo Usuario NOT FOUND!. Contact the admin!' });
  }

  // Create salt & hash
  try {
    const newUser = new User({ name, email, password });
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(newUser.password, salt);

    newUser.password = hash;
    const savedUser = await newUser.save()
    const token = jwt.sign({ id: savedUser.id }, config.get('jwtSecret'), { expiresIn: 3600 });

    res.json({
      token,
      user: {
        id: savedUser.id,
        name: savedUser.name,
        email: savedUser.email,
        tipo
      }
    });
  } catch (err) {
    console.error(err);
  }

});

// @route  DELETE api/solicitudes/:id
// @desc   delete a solicitud
// @access Public
router.delete('/:id', auth, (req, res) => {
  //TODO: Only an admin can do it
  User.findById(req.params.id)
    .then(user => user.remove().then(() => res.json({ success: true })))
    .catch(err => res.status(404).json({ success: false }))
});

export default router;

// we could use 'export default router;' in the ES6 style instead 'module.exports = router;'
// But we have to integrate babel to make sure to get a compatibility layer with
// browsers which doesn't works with ES6
