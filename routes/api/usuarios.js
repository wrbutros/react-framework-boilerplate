import { require } from '../../require.js';
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');

// Utilities
import auth from '../../middleware/auth.js';
import { massiveEntitySave, patchEdit, patchDelete } from '../utils.js';

// Models
import User, { TipoUsuario } from '../../models/User.js';

//Actions for PATCH
const DELETE_ACTION = 'delete';
const EDIT_ACTION = 'edit';

// @route  GET api/usuarios
// @desc   Get all 'user'
// @access Public
router.get('/', async (req, res) => {
  const usuarios = await User
    .find(req.query)
    .populate('tipo')
    .sort({ date: -1 });
  return res.json(usuarios);
});


const usuarioTester = async (usuario) => {
  const { name, email, password, tipo } = usuario;

  // Check fields
  if (!name || !email || !password || !tipo)
    return { res: null };

  // Check if user already exists
  const user = await User.findOne({ email });
  if (user) return { res: null, msg: 'User already exists' };

  // Check tipo usuario
  const tipoUsuario = await TipoUsuario.findOne({ _id: tipo });
  if (!tipoUsuario) return { res: null };

  // Encrypt the password
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(password, salt);
  const encryptedPass = hash;

  return {
    res: {
      name: name.toUpperCase(),
      email: email.toLowerCase(),
      password: encryptedPass,
      tipo: tipoUsuario
    }
  }
}

const usuarioTesterPrivilege = (requester) => async (userApplicant) => {
  const result = await usuarioTester(userApplicant);
  if (result.res === null) return result;
  const { isAdmin } = userApplicant;

  const requesterUser = await User.findOne({ _id: requester.id });
  // Only admin can edit this field
  if (requesterUser && requesterUser.isAdmin)
    // in case isAdmin is null, undefined or ""
    result.res.isAdmin = isAdmin || false;

  return result;
}

// @route POST api/usuarios
// @desc   Create a 'user' for each element in the array
// @access Private
router.post('/', auth, async (req, res) => {
  try {
    const {
      status, msg, result, failed, error
    } = await massiveEntitySave(req.body, User, usuarioTesterPrivilege(req.user));

    if (result === null) {
      console.log({ failed, error });
      return res.status(status).json({ msg });
    }

    res.json(result);

  } catch (error) {
    console.error(error);
    res.status(400).json({ error, msg: `Error trying to create 'usuarios'` });
  }
});

//@route  PATCH api/ usuarios/:id
//@desc   delete an 'usuario'
//@access Private
router.patch('/', auth, async (req, res) => {
  const { action, ids } = req.body;

  if (action === DELETE_ACTION) {
    await patchDelete(res, ids, User, 'user');
  } else if (action === EDIT_ACTION) {
    await patchEdit(res, ids, User, 'user');
  }
});

export default router;
