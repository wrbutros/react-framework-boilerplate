import { require } from '../../require.js';

const express = require('express');
const router = express.Router();

// Utilities
import auth from '../../middleware/auth.js';
import { massiveEntitySave } from '../utils.js';

// Models
import { TipoUsuario } from '../../models/User.js';

const tipoUsuarioTester = (tipoUsuario) => {
  const { nombre } = tipoUsuario;
  if (!nombre) return { res: null };

  return {
    res: {
      nombre: nombre.toUpperCase()
    }
  }
}

// @route  GET api/tipoUsuarios
// @desc   Get all 'tipoUsuario'
// @access Public
router.get('/', (req, res) => {
  TipoUsuario.find(req.query)
    .sort({ date: -1 })
    .then(tipoUsuarios => res.json(tipoUsuarios))
});

// @route POST api/tipoSolicitudes
// @desc   Create a tipoSolicitud
// @access Private
router.post('/', auth, async (req, res) => {
  try {
    const {
      status, msg, result, failed, error
    } = await massiveEntitySave(req.body, TipoUsuario, tipoUsuarioTester);

    if (result === null) {
      console.log({ failed, error });
      return res.status(status).json({ msg });
    }

    res.json(result);

  } catch (error) {
    console.error(error);
    res.status(400).json({ error, msg: `Error trying to create 'usuarios'` });
  }
});

export default router;

// we could use 'export default router;' in the ES6 style instead 'module.exports = router;'
// But we have to integrate babel to make sure to get a compatibility layer with
// browsers which doesn't works with ES6
