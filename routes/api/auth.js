import { require } from '../../require.js';
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const config = require('config');
const jwt = require('jsonwebtoken');

// Middleware
import auth from '../../middleware/auth.js';

// User model
import User from '../../models/User.js';

// @route  POST api/auth
// @desc   Authenticate user
// @access Public
router.post('/', async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    return res.status(400).json({ msg: 'Please enter all fields' });
  }

  // Check for existing users
  const user = await User.findOne({ email }).populate('tipo');
  if (!user) {
    return res.status(400).json({ msg: 'User does not exists' });
  }

  // Create salt & hash
  try {
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) return res.status(400).json({ msg: 'Invalid credentials' });

    const token = jwt.sign({ id: user.id }, config.get('jwtSecret'), { expiresIn: 3600 });

    res.json({
      token,
      user: {
        id: user.id,
        name: user.name,
        email: user.email,
        tipo: user.tipo,
        isAdmin: user.isAdmin
      }
    });

  } catch (err) {
    console.error(err);
  }

});

// @route  GET api/auth/user
// @desc   Get user data
// @access Private
router.get('/user', auth, (req, res) => {
  User.findById(req.user.id)
    .select('-password')
    .then(user => res.json(user));
})

export default router;

// we could use 'export default router;' in the ES6 style instead 'module.exports = router;'
// But we have to integrate babel to make sure to get a compatibility layer with
// browsers which doesn't works with ES6
