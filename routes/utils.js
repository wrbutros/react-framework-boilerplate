import mongoose from 'mongoose';

// Functions focusen in check if a given object included all the
// required fields defined in the schema.
// TODO: NOT use this functios until know how to mark a 'reference' field as required
export const collectionRequiredFields = (modelDefinition) =>
  Object.keys(modelDefinition).filter(f =>
    modelDefinition[f].required
  );
export const hasAllRequiredFields =
  (requiredFields, applicant, additionalFields = []) => {
    const allRequiredFields = [...requiredFields, additionalFields];
    const someVoid = allRequiredFields.some(f => !applicant[f]);
    return someVoid ? false : true;
  }

// ===== Helper to save entities =====

// Update an element or create it if it is new.
// If criteria is not provided use data as criteria
export const updateOrCreateIfNewFn = async (Document, data, criteria) =>
  await Document.update(
    criteria || data,
    data,
    { upsert: true }
  )

// Create an element if is new.
// If criteria is not provided use data as criteria
export const createIfNewFn = async (Document, data, criteria) => {
  const doc = await Document.findOne(data || criteria)
  if (doc) return doc;
  return await (new Document(data)).save();
}


// Every element is given to 'dataProcessorFn', then every result is
// tested with 'dataTesterFn' if evaluate true, it is given to
// 'trueCallbackFn' and get collected in an array to return it.
//
// If some execution of 'dataTesterFn' is evaluated as false
// the loop is aborted and the failed element is returned together with
// the 'dataProcessorFn' result.
const mapTrue = async (array, dataProcessorFn, dataTesterFn, trueCallbackFn) => {
  let i = 0, acc = [];
  const len = array.length;
  while (i < len) {
    const res = await dataProcessorFn(array[i]);
    if (dataTesterFn(res)) {
      acc.push(trueCallbackFn(array[i], res));
      i++;
    } else {
      return { element: array[i], res };
    }
  }
  return acc;
}

// TODO: Create a sigleSave. Autodetect if req.body is an array
// TODO: Get the entity name from the req to set it in the error messages
export const massiveEntitySave = async (entities, Document, dataProcessorFn, opt) => {
  opt = {
    dataToCheckFn: data => data && data.res,
    postProcessDataFn: null, // Should return {doc, fields} if 'savePostProcessedDataFn' is not given.
    savePostProcessedDataFn: null, // Receive 'dataToCheckFn' result. Should return the saved data.
    hookFn: null, // Executed after all the data processing. It gets the saved data.
    ...opt
  }

  if (!entities || entities.length === 0)
    return { result: null, status: 400, msg: '[Usuarios] Ingresa un array por favor' };

  const newEntities = await mapTrue(entities, dataProcessorFn, opt.dataToCheckFn,
    opt.postProcessDataFn ? opt.postProcessDataFn :
      (_, processedFields) => {
        const doc = new Document(opt.dataToCheckFn(processedFields));
        return { doc, fields: processedFields }
      });

  // Make sure to await for all the array promise
  Array.isArray(newEntities) && await Promise.all(newEntities);

  if (!Array.isArray(newEntities))
    return { result: null, status: 400, msg: newEntities.res.msg || 'Please enter all required fields', failed: newEntities.element };

  try {
    const savedEntities = await Promise.all(
      opt.saveProcessedDataFn ?
        newEntities.map(async (processedData) =>
          //await createIfNewFn(Document, entity, fields)
          await opt.saveProcessedDataFn(processedData)
        ) :
        newEntities.map(async ({ doc }) =>
          await doc.save()
        )
    );

    if (opt.hookFn) await opt.hookFn(savedEntities);

    // TODO: Populate it after save
    return { result: savedEntities, status: 200 };
  } catch (error) {
    console.error(error);
    return { result: null, status: 500, error, msg: 'Error trying to save entities array' };
  }
}


// Helper to delete an array or a single entity
export const patchDelete = async (res, ids, Document, entityName) => {
  if (!ids)
    return res.status(400).json({ msg: `Please send which ${entityName} you want to delete` });

  const entities = Array.isArray(ids) ? ids : [ids];

  try {
    const entityIds = entities.map(id => mongoose.mongo.ObjectId(id));
    await Document.deleteMany({
      "_id": {
        $in: entityIds
      }
    })
    return res.json({ success: true })
  } catch (error) {
    console.error(error);
    return res.status(400).json({ error, msg: `Error trying to delete ${entityName}` });
  }
};


export const patchEdit = async (res, ids, Document) => {
  console.log("Im doing NOTHING!");
};
