// GENERIC ONCHANGE
export const genericOnChange = (component) => (e) => {
  component.setState({
    [e.target.name]: e.target.value
  })
}

export const getCleanStatesFields = (fields) => {
  const defaultValue = (field) => {
    switch (field.type) {
      case 'boolean':
        return field.default || false;
      default:
        return "";
    }
  };

  if (fields) {
    const cleanedFields = fields.reduce((prev, f) => ({ ...prev, [f.selector]: defaultValue(f) }), {});
    return cleanedFields;
  } else {
    console.error('THIS COMPONENT IS NOT GETTING FIELDS');
    return {};
  }
}
