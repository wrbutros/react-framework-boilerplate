// This file is a helper for formularies elements which needs to be fetched from the server
import axios from 'axios';
import { objectToQueryString, loadQuery } from './utils';
import { storedTokenConfig } from './header';

// Gets a generic entity
export const getEntity = async (endpoint, { GETPath = '', query }) => {
  const localQuery = await loadQuery(query);
  const queryURL = objectToQueryString(localQuery);
  try {
    const res = await axios.get(`/api/${endpoint}/${GETPath}${queryURL}`, storedTokenConfig());
    return res.data;
  } catch (err) {
    console.error(err);
  }
}
