import moment from 'moment';
import qs from 'qs';
const _ = require('lodash');

// ENTITIES utilities
export const dateFormatFn = (f, format = 'DD/MM/YYYY HH:mm:ss') => moment(f).format(format);
export const booleanFormatFn = (f) => `${(f === null || f === undefined) ? '' : f ? 'SI' : 'NO'}`;

// CAPITALIZE TEXT
export const capitalize = (str, lower = false) =>
  (lower ? str.toLowerCase() : str)
    .replace(/(?:^|\s|["'([{])+\S/g, match => match.toUpperCase());

// TRANSFORM OBJECT INTO A GET URL PARAMETERS
export const objectToQueryString = (obj) =>
  obj && !_.isEmpty(obj) ? '?' + qs.stringify(obj) : ''

// SUB-FIELD SELECTOR
export const fieldSelector = (pathField) => (obj) => {
  if (!obj) return 'NOT FOUND';
  if (typeof obj === 'string') return obj;
  const path = pathField.split('.');
  return path.reduce((currField, key) => currField ? (currField[key] || 'NOT FOUND') : 'NOT FOUND', obj);
}

// MAKE SURE TO RETURN THE QUERY OBJECT
export const loadQuery = async (query) =>
  typeof query === 'function' ? await query() : query;

// FUSION FIELDS
export const fusionFields = (pathFields) => (obj) => {
  if (!obj || _.isEmpty(obj)) return 'NOT FOUND';
  const fields = pathFields.split(',').map(f => f.trim());
  return joinColumns(obj, fields);
}

// FILTERS
const AND_TOKEN_FILTER = '&';
const OR_TOKEN_FILTER = '|';
const NO_TOKEN_FILTER = '';

// Generic Table static data and function
const joinColumns = (obj, columns) =>
  columns.reduce((acc, column) => acc ? `${acc} ${fieldSelector(column)(obj)}` : fieldSelector(column)(obj), '');

// NORMAL FILTER
const normalFilter = (data, columns, filter) =>
  data.filter(item => joinColumns(item, columns).toLowerCase().includes(filter.toLowerCase()))

// AND FILTER
const andFilter = (data, columns, filters) =>
  filters.reduce((filteredData, currentFilter) =>
    normalFilter(filteredData, columns, currentFilter)
    , data);

// OR FILTER
const orFilter = (data, columns, filters) => {
  const validFilters = filters.filter(f => f);
  if (!validFilters.length) return data;

  return validFilters.reduce((filteredData, currentFilter) => {
    const currentData = validFilters[0] === currentFilter
      ? [] : normalFilter(data, columns, currentFilter);

    return [...filteredData, ...currentData];
  }, normalFilter(data, columns, validFilters[0]))
}


// AUTO DETECT FILTER
export const specialFilter = (data, columns, filterText) => {
  if (!filterText) return data;

  let detectedFilter =
    // IF
    filterText.includes(AND_TOKEN_FILTER) ?
      AND_TOKEN_FILTER :
      // ELSE IF
      filterText.includes(OR_TOKEN_FILTER) ?
        OR_TOKEN_FILTER :
        // ELSE
        NO_TOKEN_FILTER;

  switch (detectedFilter) {
    case AND_TOKEN_FILTER:
      return andFilter(data, columns, filterText.split(AND_TOKEN_FILTER));
    case OR_TOKEN_FILTER:
      return orFilter(data, columns, filterText.split(OR_TOKEN_FILTER));
    default:
      return normalFilter(data, columns, filterText);
  }
}
