export const ESTADO_RUTAS = 'ESTADO_RUTAS';
export const ESTADO_ORDENES = 'ESTADO_ORDENES';
export const TIPO_USUARIOS = 'TIPO_USUARIOS';
export const ROL_REPARTIDORES = 'ROL_REPARTIDORES';
export const TOKEN = 'TOKEN';

export const addToStorage = async (nombreItem, valueItem) => {
  localStorage.setItem(nombreItem, valueItem);
}

export const removeFromStorage = (itemName) => {
  localStorage.removeItem(itemName);
}

export const getFromStorage = (itemName) => {
  return localStorage.getItem(itemName);
}
