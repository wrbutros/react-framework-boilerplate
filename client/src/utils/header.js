import { TOKEN, getFromStorage } from '../utils/storage';

// Return a json header for 'application/json' content type
export const jsonHeaders = () => {
  return {
    headers: {
      'content-type': 'application/json'
    }
  }
}

// Setup config/headers and token
export const tokenConfig = getState => {
  // Get token from localstorage
  const token = getState().auth.token;
  // Headers
  const config = jsonHeaders();
  // If token add headers
  if (token) {
    config.headers['x-auth-token'] = token;
  }

  return config;
}


// Setup config/headers and token
export const storedTokenConfig = () => {
  // Get token from localstorage
  const token = getFromStorage(TOKEN);
  // Headers
  const config = jsonHeaders();
  // If token add headers
  if (token) {
    config.headers['x-auth-token'] = token;
  }

  return config;
}
