import { fieldSelector } from './utils';

export const convertArrayOfObjectsToCSV = (array, columns, columnDelimiter = ';') => {
  let result;

  const lineDelimiter = '\n';
  const keys = columns || Object.keys(array[0]);

  result = '';
  result += keys.join(columnDelimiter);
  result += lineDelimiter;

  array.forEach(item => {
    let ctr = 0;
    keys.forEach(key => {
      if (ctr > 0) result += columnDelimiter;

      result += fieldSelector(key)(item);

      ctr++;
    });
    result += lineDelimiter;
  });

  return result;
}

export const downloadCSV = (array, columns, filename = 'export.csv') => {
  const link = document.createElement('a');
  let csv = convertArrayOfObjectsToCSV(array, columns);
  if (csv == null) return;

  if (!csv.match(/^data:text\/csv/i)) {
    csv = `data:text/csv;charset=utf-8,${csv}`;
  }

  link.setAttribute('href', encodeURI(csv));
  link.setAttribute('download', filename);
  link.click();
}
