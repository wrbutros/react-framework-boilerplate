import { dateFormatFn, booleanFormatFn } from '../utils/utils';

const TipoUsuarioEntity = {
  name: 'tipoUsuario',
  endpoint: 'tipoUsuarios',
  referenceColumn: 'nombre',

  tableColumns: [{
    name: 'ID',
    selector: '_id',
    omit: true,
  }, {
    name: 'Nombre',
    selector: 'nombre',
    sortable: true,
  }, {
    name: 'Activo',
    selector: 'activo',
    format: ({ activo: a }) => booleanFormatFn(a),
    sortable: true,
  }, {
    name: 'Creado en',
    selector: 'createdAt',
    format: ({ createdAt: d }) => dateFormatFn(d),
    sortable: true,
  }],

  fields: [{
    name: 'Nombre',
    selector: 'nombre',
    type: 'text',
    required: true
  }]
}

export default TipoUsuarioEntity;
