import UsuarioEntity from './UsuarioEntity';
import TipoUsuarioEntity from './TipoUsuarioEntity';

export {
  UsuarioEntity,
  TipoUsuarioEntity
}
