import {
  getFromStorage,
  addToStorage,
  TIPO_USUARIOS,
} from '../../utils/storage';
import { getEntity } from '../../utils/request';

const entities = {
  tipoUsuarios: {
    storage: TIPO_USUARIOS,
    endpoint: 'tipoUsuarios',
    query: { activo: true }
  }
}

const fetchAndSaveEntity = async (entity, cache = true) => {
  try {
    const entityData = await getEntity(entity.endpoint, entity.query);
    cache && addToStorage(entity.storage, entityData);
    return entityData;
  } catch (error) {
    console.error(error);
  }
}

const fetchOrGet = async (entity, cache = true) => {
  if (!cache) return await fetchAndSaveEntity(entity, cache);

  let entityData = getFromStorage(entity.storage);
  entityData = typeof entityData !== 'string' ? entityData : null;
  if (!entityData) {
    entityData = await fetchAndSaveEntity(entity);
  }
  return entityData;
}

// const rutaActivas = async () => {
//   const estadoRutas = await fetchOrGet(entities.estadoRutas);
//   const estadoFinalizado = estadoRutas && estadoRutas.find(er => er.nombre === 'FINALIZADO');
//   const rutaNoFinalizadoFilter = estadoFinalizado ? { estado: { '$ne': estadoFinalizado._id } } : {};
//   return { 'ordenes.0': { '$exists': true }, ...rutaNoFinalizadoFilter };
// }

export default {
  //solicitudesNoIngresadas: { $or: [{ ordenesAsignadas: 0 }, { reintentoPendiente: true }] },
  //ordenesPorVerificar,
}
