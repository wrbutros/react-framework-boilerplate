import { dateFormatFn, booleanFormatFn } from '../utils/utils';

const UsuarioEntity = {
  name: 'usuario',
  endpoint: 'usuarios',
  referenceColumn: 'email', //TODO: make it multicolumn, the utils should have the 'not found' text as optional param

  tableColumns: [{
    name: 'ID',
    selector: '_id',
    omit: true,
  }, {
    name: 'Nombre',
    selector: 'name',
    sortable: true,
  }, {
    name: 'Email',
    selector: 'email',
    sortable: true,
  }, {
    name: 'Tipo',
    selector: 'tipo.nombre',
    sortable: true,
  }, {
    name: 'Admin',
    selector: 'isAdmin',
    format: ({ isAdmin: a }) => booleanFormatFn(a),
    sortable: true,
  }, {
    name: 'Activo',
    selector: 'activo',
    format: ({ activo: a }) => booleanFormatFn(a),
    sortable: true,
  }, {
    name: 'Creado en',
    selector: 'createdAt',
    format: ({ createdAt: d }) => dateFormatFn(d),
    sortable: true,
  }],

  fields: [{
    name: 'Nombre',
    selector: 'name',
    type: 'text',
    required: true
  }, {
    name: 'Email',
    selector: 'email',
    type: 'email',
    required: true
  }, {
    name: 'Password',
    selector: 'password',
    type: 'password',
    required: true
  }, {
    name: 'Tipo',
    selector: 'tipo',
    type: 'reference',
    required: true,
    reference: {
      name: 'tipoUsuario',
      endpoint: 'tipoUsuarios',
      select: '_id',
      show: 'nombre',
      filter: {
        activo: true
      }
    }
  }, {
    name: 'Admin',
    selector: 'isAdmin',
    type: 'boolean',
    default: false,
    onlyAdmin: true
  }]
}

export default UsuarioEntity;
