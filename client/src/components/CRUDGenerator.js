import { connect } from 'react-redux';
import { AUTH_ERROR, DEL_FAIL_ENTITY, ADD_FAIL_ENTITY } from '../actions/types';
import GenericCRUD from './generics/GenericCRUD';

// ENTITIES
import * as E from '../entities';

// SPECIALS
import * as S from '../entities/specials';

// Queries
import Q from '../entities/queries';

import {
  getEntitiesAction,
  setSelectedEntityAction,
  deleteEntityAction,
  addEntityAction
} from '../actions/entityActionsGenerator';

// Function to generate CRUD connected with react-redux for an specified entity.
// entity: entity to create CRUD
// title: title of list
// GETPath: an aditional path to get the element list /api/{entityName}/{GETPath}
// query: a query to filter the elements shown in the list /api/{entityName}/{GETPath}/{filter}
//        recives an object and automatically parse it to url query format
// stateField: name of the redux state to save and get the states of the entity
// additionalTableProps: object with customization options for GenericTable
// additionalTableActions: function to render into the actions for selected items in the table
// allowedActions: let you enable or disable CRUD actions: (add, delete, export and select)
// style: style for CRUD cointainer
const generateCRUD = ({
  entity, title, GETPath, query, stateField, additionalTableProps,
  additionalTableActions, allowedActions, preFilterFn, style
}) => {
  allowedActions = { add: true, delete: true, export: false, select: {}, ...allowedActions };
  allowedActions.select = { enable: true, pageOnly: false, rowDisabledCriteria: null, ...allowedActions.select }

  const actionsAndStateName = stateField || entity.name;
  // The state.solicitud refers to the 'entity reducer' defined in 'combineReducers'
  const mapStateToProps = (state) => ({
    entityState: state[actionsAndStateName],
    errorState: state.error,
    auth: state.auth
  });

  // Generate Action Functions for entity
  const getEntities = getEntitiesAction(actionsAndStateName, entity.endpoint, { GETPath, query });
  const selectedEntities = setSelectedEntityAction(actionsAndStateName);
  const deleteEntity = deleteEntityAction(actionsAndStateName, entity.endpoint);
  const addEntity = addEntityAction(actionsAndStateName, entity.endpoint);

  const DEL_FAIL_FOR_THIS_ENTITY = `${DEL_FAIL_ENTITY}${actionsAndStateName.toUpperCase()}`;
  const ADD_FAIL_FOR_THIS_ENTITY = `${ADD_FAIL_ENTITY}${actionsAndStateName.toUpperCase()}`;

  const signalsToWatch = [AUTH_ERROR, DEL_FAIL_FOR_THIS_ENTITY, ADD_FAIL_FOR_THIS_ENTITY];

  const mapDispatchToProps =
    (dispatch,
      ownProps = {
        entity, title, signalsToWatch, additionalTableProps,
        additionalTableActions, allowedActions, preFilterFn, style
      }) => ({
        entity: ownProps.entity,
        title: ownProps.title !== undefined ? ownProps.title : ownProps.entity.name,
        signalsToWatch: ownProps.signalsToWatch,
        additionalTableProps: ownProps.additionalTableProps,
        additionalTableActions: ownProps.additionalTableActions,
        allowedActions: ownProps.allowedActions,
        preFilterFn: ownProps.preFilterFn,
        style: ownProps.style,
        getEntities: () => dispatch(getEntities()),
        selectedEntities: (selected) => dispatch(selectedEntities(selected)),
        deleteEntity: (id) => dispatch(deleteEntity(id)),
        addEntity: (entity) => dispatch(addEntity(entity))
      });

  // This connect receives
  // a function to map the redux states into props for this component,
  // a props list which are additional props
  return connect(mapStateToProps, mapDispatchToProps)(GenericCRUD);
}

// Componentes de procedimientos

// export const RutasDisponiblesVizualizer = generateCRUD({
//   entity: S.RutaVisualizer,
//   title: 'Rutas Activas',
//   query: Q.rutaActivas,
//   allowedActions: {
//     delete: false, add: false, select: {
//       enable: true,
//       rowDisabledCriteria: r => r.estado.nombre !== 'EN ESPERA'
//     }
//   },
//   additionalTableActions: RevertRutaToOrdenButton,
//   additionalTableProps: {
//     striped: false,
//     conditionalRowStyles: [{
//       when: row => row.estado.nombre !== 'EN ESPERA',
//       style: {
//         color: 'rgba(0,0,0,.54)',
//         '&:hover': {
//           cursor: 'not-allowed',
//         },
//       },
//     }]
//   },
//   style: {}
// });


// Mantenedores
export const TipoUsuarioCRUD = generateCRUD({
  entity: E.TipoUsuarioEntity,
  query: { activo: 'true' }
});

export const UsuarioCRUD = generateCRUD({
  entity: E.UsuarioEntity,
  allowedActions: { delete: false }
});
