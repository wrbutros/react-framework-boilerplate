import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import {
  Button,
  Form,
  FormGroup,
  Label,
  Input
} from 'reactstrap';
import { Dropdown, Checkbox } from 'semantic-ui-react'

import { capitalize, fusionFields } from '../../utils/utils';
import { getEntity } from '../../utils/request';

const FieldInputText = ({ formName, field, onChange, autoFocus, value }) => (
  <Input
    id={`input_${formName}_${field.selector}`}
    name={field.selector}
    type={field.type}
    placeholder={capitalize(field.name)}
    className="mb-3"
    onChange={onChange}
    autoFocus={autoFocus}
    value={value} />
);

export const GenericDropDownList = ({ fieldName, onChange, data, keyField, valueField, textField, value, required }) => {
  const options = data && data.length > 0 ?
    data.map(opt => ({
      key: opt[keyField],
      value: opt[valueField],
      text: fusionFields(textField)(opt)
    })) : [];

  return (
    <Dropdown
      className="mb-3"
      placeholder={`Select ${fieldName}`}
      fluid
      search
      selection
      options={options}
      onChange={onChange}
      value={value}
      clearable={!required}
    />
  );
}

export class GenericForm extends Component {
  state = {}
  fetchedFields = null;

  // Local callback to cast the semanticUI element's onChange returned data
  // into the required data for the given onChange callback
  semanticUIData = (field, value) => {
    this.props.onChange({
      target: {
        name: field.selector,
        value: value
      }
    });
  }

  dropDownListOnChange = (field) => (event, data) => this.semanticUIData(field, data.value);
  checkboxOnChange = (field) => (event, data) => this.semanticUIData(field, data.checked);

  componentDidMount = async () => {
    await this.fetchReferenceFields();
  }

  fetchReferenceFields = async () => {
    // Fetching all the reference fields
    const { fields } = this.props;
    const referenceFiels = fields.filter(f => f.type === 'reference')
    const fetchedFieldsArr = await Promise.all(referenceFiels.map(
      async f => {
        const data = await getEntity(f.reference.endpoint, { query: f.reference.filter });
        return { [f.selector]: { data } }
      }));

    const fetchedFields = fetchedFieldsArr.reduce((prev, curr) => ({ ...prev, ...curr }), {});
    this.fetchedFields = fetchedFields;
    this.setState({ refreshToggle: !this.state.refreshToggle });
  }

  onSubmit = async (e) => {
    await this.props.onSubmit(e);
    if (this.props.refetchFieldsOnSubmit)
      await this.fetchReferenceFields();
  }

  getFetchedFieldData = (field) => (
    this.fetchedFields &&
      this.fetchedFields[field.selector] &&
      this.fetchedFields[field.selector].data &&
      this.fetchedFields[field.selector].data.length > 0 ?
      this.fetchedFields[field.selector].data : []
  )

  render() {
    const { name, fields, onChange, actionName, isAdmin, values } = this.props;
    const allowedFields = fields.filter(f => !(f.onlyAdmin && !isAdmin));
    let autoFocus = false;

    return (
      <Form onSubmit={this.onSubmit}>
        <FormGroup>
          {allowedFields.map((field, key) => (
            <Fragment key={`generic_form_${key}`}>
              <Label
                id={`label_${name}_${field.selector}`}
                for={field.selector}>
                {
                  `${capitalize(field.name)}${field.required ? ' (*)' : ''}`
                }
              </Label>

              {/*TextField*/}
              {(field.type === 'text' || field.type === 'email' || field.type === 'password') && (
                <FieldInputText {...{
                  formName: name,
                  field,
                  onChange,
                  value: values && values[field.selector],
                  autoFocus: (!autoFocus ? autoFocus = true && true : false)
                }} />
              )}

              {/*CheckBox*/}
              {field.type === 'boolean' && (
                <div style={{ display: 'block' }}>
                  <Checkbox onChange={this.checkboxOnChange(field)} toggle />
                </div>
              )}

              {/*Reference Field (selector)*/}
              {field.type === 'reference' && (
                <GenericDropDownList {...{
                  fieldName: field.name,
                  data: this.getFetchedFieldData(field),
                  keyField: field.reference.select,
                  valueField: field.reference.select,
                  textField: field.reference.show,
                  onChange: this.dropDownListOnChange(field),
                  value: values && values[field.selector],
                  required: field.required
                }} />
              )}
            </Fragment>
          ))}
          <Button color="dark" style={{ marginTop: '2rem' }} block>
            {actionName || `Agregar ${capitalize(name)}`}
          </Button>
        </FormGroup>
      </Form>
    );
  }
};

GenericForm.propTypes = {
  name: PropTypes.string.isRequired,
  fields: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  refetchFieldsOnSubmit: PropTypes.bool,
  actionName: PropTypes.string,
  isAdmin: PropTypes.bool,
  values: PropTypes.object
}
