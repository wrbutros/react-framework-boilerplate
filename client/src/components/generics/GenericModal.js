import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Button, Icon } from 'semantic-ui-react';
import {
  Alert,
  Modal,
  ModalHeader,
  ModalBody,
} from 'reactstrap';

import { GenericForm } from './GenericForm';
import { capitalize } from '../../utils/utils';
import { getCleanStatesFields, genericOnChange } from '../../utils/form';

const ModalButton = props => (
  <Button
    color="green"
    style={{ marginBottom: '2rem' }}
    onClick={props.toggle}>
    <Icon name='plus' /> {`Agregar ${capitalize(props.name)}`}
  </Button>
);

class GenericModal extends Component {
  state = {
    modal: false
  }

  cleanStates = getCleanStatesFields(this.props.entity.fields);

  // HOF = High Order Function
  toggleHOF = (optionalStates = {}) => () => {
    this.setState({
      ...optionalStates,
      modal: !this.state.modal
    });
  }

  onSubmit = async (e) => {
    e.preventDefault();
    const { toggleHOF, cleanStates, props: { actionFn } } = this;

    let formFields = {};
    this.props.entity.fields.forEach(f =>
      formFields[f.selector] = this.state[f.selector]
    );

    // Add Entity
    const res = await actionFn(formFields);
    if (res)
      toggleHOF(cleanStates)();
  }

  renderModalButton = (toggle, name) =>
    this.props.alternativeButton ?
      this.props.alternativeButton(toggle) :
      <ModalButton toggle={toggle} name={name} />


  render() {
    const { entity: { name, fields }, errorMsg, isAdmin } = this.props;
    const { state: { modal }, toggleHOF, cleanStates } = this;
    const toggleAndCleanFn = toggleHOF(cleanStates);

    return (
      <>
        {this.props.isAuthenticated ?
          this.renderModalButton(toggleHOF(), name) :
          <h4 className="mb-3 ml4">{`Identificate para administrar ${name}`}</h4>
        }
        <Modal
          isOpen={modal}
          autoFocus={false}
          toggle={toggleAndCleanFn}>
          <ModalHeader toggle={toggleAndCleanFn}>{`Agregar ${name}`}</ModalHeader>
          <ModalBody>
            {errorMsg && <Alert color="danger">{errorMsg}</Alert>}
            <GenericForm {...{ name, fields, onChange: genericOnChange(this), onSubmit: this.onSubmit, isAdmin }} />
          </ModalBody>
        </Modal>
      </>
    );
  }
}

GenericModal.propTypes = {
  entity: PropTypes.object.isRequired,
  actionFn: PropTypes.func.isRequired,
  alternativeButton: PropTypes.func,
  errorMsg: PropTypes.string,
  isAuthenticated: PropTypes.bool,
  isAdmin: PropTypes.bool
}

export default GenericModal;
