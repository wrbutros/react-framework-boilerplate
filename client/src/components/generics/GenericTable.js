import React from 'react';
import DataTable, { createTheme } from 'react-data-table-component';
import { Button, InputGroup, InputGroupAddon, Input } from 'reactstrap';
import {Button as SemanticBtn, Icon } from 'semantic-ui-react';

import { specialFilter } from '../../utils/utils';
import { downloadCSV } from '../../utils/export';

createTheme('invertedHeader', {
  context: {
    background: '#23272b',
    text: '#F8F8EB',
  },
});

const FilterComponent = ({ filterText, onFilter, onClear }) => (
  <>
    <InputGroup>
      <Input id="search" type="text" placeholder="Filtro general" value={filterText} onChange={onFilter} />
      <InputGroupAddon addonType="append">
        <Button onClick={onClear}>&times;</Button>
      </InputGroupAddon>
    </InputGroup>
  </>
);

const Export = ({ onExport }) => (
  <SemanticBtn color='black' onClick={e => onExport(e.target.value)}>
    <Icon name='download' />Export
  </SemanticBtn>
);

const GenericTable = ({
  // Required
  columns,
  tableData,
  // Optionals
  addAction,
  deleteAction,
  secondaryAction,
  allowedActions,
  tableName = "",
  onSelectedRowsChange,
  selectablePageOnly = false,
  rowDisabledCriteria = null,
  rowColumnReference = "",
  expandableRowsComponent,
  additionalTableProps
}) => {
  // === Initialize states and its 'setters functions' sub-components ===
  // NOTE: useState(initialValue) returns an 'state' var and an asociated 'setter' function
  // FilterText text
  const [filterText, setFilterText] = React.useState('');
  // Pagination state
  const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);
  // Selected rows
  const [selectedRows, setSelectedRows] = React.useState([]);
  const [toggleCleared, setToggleCleared] = React.useState(false);
  const filterColumns = columns.map(c => c.selector);

  allowedActions = { add: true, delete: true, export: false, select: false, ...allowedActions };
  const { add: canAdd, delete: canDelete, select: canSelect, export: canExport} = allowedActions;

  // Similar to ComponentDidMount
  React.useEffect(() => {
    setToggleCleared(!toggleCleared);
    setResetPaginationToggle(!resetPaginationToggle);
    // Example: Subscribes, timers...
    // Preparing data as appear in the table data
    //    return () => {
    // similar to componentWillUnmount
    // Clean subscriptions here.
    //    };
    // TODO: Solve warning because no 'toggleCleared' (if add it to the array got into a loop)
  }, [tableData]);

  // Filter items by checking if filterText is contained into the item's joined fields
  //const filteredItems = tableData.filter(item => joinColumns(item).toLowerCase().includes(filterText.toLowerCase()));
  const filteredItems = specialFilter(tableData, filterColumns, filterText);

  // Fn = Create a callback for row selection
  const handleRowSelected = React.useCallback(state => {
    setSelectedRows(state.selectedRows);
    onSelectedRowsChange(state.selectedRows);
  }, [onSelectedRowsChange]);

  // Fn = Create filter component for subheader
  const subHeaderComponentMemo = React.useMemo(() => {
    const handleClear = () => {
      if (filterText) {
        setResetPaginationToggle(!resetPaginationToggle);
        setFilterText('');
      }
    };

    return <FilterComponent onFilter={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} />;
  }, [filterText, resetPaginationToggle]);


  const actions = React.useMemo(() =>
    <div>
      {canExport && <Export onExport={() => downloadCSV(filteredItems, columns.map(c => c.selector))} />}
      {canAdd && addAction}
    </div>
    , [filteredItems, columns, canAdd, canExport, addAction]);

  // Fn = create a component to delete selected fields
  const contextActions = React.useMemo(() => {
    const getSelectedData = () => {
      const idColumn = "_id";
      // Show the idColumn if the rowColumnReference was not provided
      const rcr = rowColumnReference || idColumn;
      // Function to select the data to show (depending on if it has data)
      const selectRowColumnFn = r => r[rcr] || r[idColumn];

      // Getting the rows id (to work on it)
      // Getting the rows reference column (to show as message)
      const { refs, ids } = selectedRows.reduce(
        (prev, curr) => ({
          refs: [...prev.refs, selectRowColumnFn(curr)],
          ids: [...prev.ids, curr[idColumn]]
        }), { refs: [], ids: [] });

      return { ids, refs };
    };
    return (
      <div>
        {secondaryAction && secondaryAction({ selected: selectedRows })}
        {canDelete && deleteAction()(getSelectedData)}
        {/* TODO: Check this file in 6e6d967f0c90f7c8c45609174791595ae8950c46
          * REALY STRANGE ERROR:
          * If I execute deleteAction={this.deleteButton()} in GenericCRUD and then
          * deleteAction(getSelectedData) in here, the 'secondaryAction' button cannot
          * refresh its own state.
          * */}
      </div>
    );
  }, [selectedRows, deleteAction, canDelete, rowColumnReference, secondaryAction]);

  const paginationOptions = { rowsPerPageText: 'Filas por página', rangeSeparatorText: 'de', selectAllRowsItem: true, selectAllRowsItemText: 'Todos' };

  return (
    <DataTable {...{
      title: tableName,
      columns,
      data: filteredItems,
      theme: "invertedHeader",
      pagination: true,
      paginationResetDefaultPage: resetPaginationToggle, // optionally, a hook to reset pagination to page 1
      subHeader: true,
      subHeaderComponent: subHeaderComponentMemo,
      selectableRows: canSelect,
      selectableRowDisabled: rowDisabledCriteria,
      persistTableHead: true,
      contextActions,
      onSelectedRowsChange: handleRowSelected,
      clearSelectedRows: toggleCleared,
      striped: true,
      responsive: true,
      expandableRows: expandableRowsComponent ? true : false,
      expandOnRowClicked: expandableRowsComponent ? true : false,
      selectableRowsVisibleOnly: selectablePageOnly,
      paginationComponentOptions: paginationOptions,
      expandableRowsComponent: expandableRowsComponent && expandableRowsComponent(),
      ...{ ...additionalTableProps, actions },
    }} />
  )
}

export default GenericTable;
