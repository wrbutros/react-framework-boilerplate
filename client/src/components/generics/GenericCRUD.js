import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'reactstrap';
//import Card from '@material-ui/core/Card';

import GenericModal from './GenericModal';
import GenericTable from './GenericTable';
import { capitalize } from '../../utils/utils';
import { Button, Icon } from 'semantic-ui-react';

class GenericCRUD extends Component {
  state = {
    msg: ""
  }

  componentDidMount() {
    this.props.getEntities();
  }

  componentDidUpdate(prevProps) {
    const { errorState } = this.props;
    // Check if ther is a change
    if (errorState !== prevProps.errorState) {
      // Chek for register error
      this.setState({ msg: errorState.msg.msg });
      setTimeout(() => { this.setState({ msg: "" }) }, 5000);
    }
  }

  onAddEntity = (addEntity) => async (fields) => {
    const res = await addEntity(fields);
    if (res && res.status === 200)
      return true;
    else return false;
  }

  onDeleteEntity = (deleteEntities, getSelectedData) => async () => {
    const { ids, refs } = getSelectedData();
    if (window.confirm(`Seguro que deseas eliminar: \r ${refs} ? `)) {
      await deleteEntities(ids);
    }
  }

  // TODO: Ugly params, redisign this
  addButton = (user, entity, addEntityFn, errorMsg, isAuthenticated) =>
    <GenericModal
      entity={entity}
      actionFn={this.onAddEntity(addEntityFn)}
      errorMsg={errorMsg}
      isAuthenticated={isAuthenticated}
      isAdmin={user && user.isAdmin}
    />

  deleteButton = () => (getSelectedData) =>
    <Button key="delete" basic inverted color="red" onClick={this.onDeleteEntity(this.props.deleteEntity, getSelectedData)}>
      <Icon name='trash' />Eliminar
    </Button>

  render() {
    const {
      entity,
      title,
      entityState: { list: entityList, loading },
      //deleteEntity,
      addEntity,
      selectedEntities,
      errorState,
      auth: { isAuthenticated, user },
      signalsToWatch,
      additionalTableProps,
      additionalTableActions,
      allowedActions,
      expandableRowsComponent,
      preFilterFn,
      style
    } = this.props;
    const preFilteredEntityList = preFilterFn ? entityList.filter(preFilterFn) : entityList;
    const addButton = this.addButton(user, entity, addEntity, this.state.msg, isAuthenticated);
    //const deleteButton = this.deleteButton(deleteEntity); TODO: Details in GenericTable
    const allowedTableActions = {
      add: isAuthenticated && allowedActions.add,
      delete: isAuthenticated && allowedActions.delete,
      export: isAuthenticated && allowedActions.export,
      select: isAuthenticated && allowedActions.select.enable
    };
    return (
      <div style={style ? style : { marginInline: '2em' }}>
        {this.state.msg && signalsToWatch.includes(errorState.id) && <Alert color="danger">{this.state.msg}</Alert>}
        {loading === true && <Alert color="warning">Loading...</Alert>}
        {/* Generic Table */}
        <GenericTable
          columns={entity.tableColumns}
          tableData={preFilteredEntityList}
          addAction={addButton}
          deleteAction={this.deleteButton}
          allowedActions={allowedTableActions}
          onSelectedRowsChange={selectedEntities}
          tableName={capitalize(title)}
          selectablePageOnly={allowedActions.select.pageOnly}
          rowDisabledCriteria={allowedActions.select.rowDisabledCriteria}
          rowColumnReference={entity.referenceColumn}
          secondaryAction={additionalTableActions}
          expandableRowsComponent={expandableRowsComponent}
          additionalTableProps={additionalTableProps}
        />
      </div >
    );
  }
}

GenericCRUD.propTypes = {
  // Global States
  entityState: PropTypes.object.isRequired,
  errorState: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  // Redux Actions
  getEntities: PropTypes.func.isRequired,
  selectedEntities: PropTypes.func.isRequired,
  deleteEntity: PropTypes.func.isRequired,
  addEntity: PropTypes.func.isRequired,
  // Aditional
  entity: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  signalsToWatch: PropTypes.array.isRequired,
  allowedActions: PropTypes.object.isRequired,
  additionalTableProps: PropTypes.object,
  additionalTableActions: PropTypes.func,
  expandableRowsComponent: PropTypes.func,
  preFilterFn: PropTypes.func,
  style: PropTypes.object
}

export default GenericCRUD;
