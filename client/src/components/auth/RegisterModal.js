import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  NavLink,
  Alert
} from 'reactstrap';
import { connect } from 'react-redux';
import { register } from '../../actions/authActions';
import { clearErrors } from '../../actions/errorActions';
import { REGISTER_FAIL } from '../../actions/types';


const UserForm = ({ onChange, onSubmit }) => {
  return (
    <Form onSubmit={onSubmit}>
      <FormGroup>
        {/* Name */}
        <Label for="name">Name</Label>
        <Input
          name="name"
          type="text"
          id="name"
          placeholder="Name"
          className="mb-3"
          onChange={onChange} />

        {/* Email */}
        <Label for="email">Email</Label>
        <Input
          name="email"
          type="email"
          id="email"
          placeholder="Email"
          className="mb-3"
          onChange={onChange} />

        {/* Password */}
        <Label for="password">Password</Label>
        <Input
          name="password"
          type="password"
          id="password"
          placeholder="password"
          className="mb-3"
          onChange={onChange} />

        {/* Register Button */}
        <Button color="dark" style={{ marginTop: '2rem' }} block>
          Register
        </Button>
      </FormGroup>
    </Form>
  );
}

class RegisterModal extends Component {
  state = {
    modal: false,
    name: '',
    email: '',
    password: '',
    msg: null
  }

  componentDidUpdate(prevProps) {
    const { error, isAuthenticated } = this.props;
    // Check if ther is a change
    if (error !== prevProps.error) {
      // Chek for register error
      if (error.id === REGISTER_FAIL) {
        this.setState({ msg: error.msg.msg });
      } else {
        this.setState({ msg: null });
      }
    }

    // If get authenticated close modal
    if (this.state.modal && isAuthenticated) {
      this.toggle();
    }
  }

  toggle = () => {
    // Clear errors
    this.props.clearErrors();

    this.setState({
      modal: !this.state.modal
    });
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  onSubmit = (e) => {
    e.preventDefault();

    const { name, email, password } = this.state;

    // Create newUser object
    const newUser = { name, email, password };
    // Attempt to register
    this.props.register(newUser);
    this.toggle();
  }

  render() {
    return (
      <div>
        <NavLink onClick={this.toggle} href="#">
          Register
        </NavLink>

        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>Register</ModalHeader>
          <ModalBody>
            {this.state.msg && <Alert color="danger">{this.state.msg}</Alert>}
            <UserForm onSubmit={this.onSubmit} onChange={this.onChange} />
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

RegisterModal.propTypes = {
  isAuthenticaded: PropTypes.bool,
  error: PropTypes.object.isRequired,
  register: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.error
});

export default connect(mapStateToProps, { register, clearErrors })(RegisterModal);
