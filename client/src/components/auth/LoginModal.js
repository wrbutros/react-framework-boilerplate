import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  NavLink,
  Alert
} from 'reactstrap';
import { connect } from 'react-redux';
import { login } from '../../actions/authActions';
import { clearErrors } from '../../actions/errorActions';
import { LOGIN_FAIL } from '../../actions/types';


const LoginForm = ({ onChange, onSubmit }) => {
  return (
    <Form onSubmit={onSubmit}>
      <FormGroup>
        {/* Email */}
        <Label for="email">Email</Label>
        <Input
          name="email"
          type="email"
          id="email"
          placeholder="Email"
          className="mb-3"
          onChange={onChange} />

        {/* Password */}
        <Label for="password">Password</Label>
        <Input
          name="password"
          type="password"
          id="password"
          placeholder="password"
          className="mb-3"
          onChange={onChange} />

        {/* Register Button */}
        <Button color="dark" style={{ marginTop: '2rem' }} block>
          Login
        </Button>
      </FormGroup>
    </Form>
  );
}

class LoginModal extends Component {
  state = {
    modal: false,
    email: '',
    password: '',
    msg: null
  }

  componentDidUpdate(prevProps) {
    const { error, isAuthenticated } = this.props;
    // Check if ther is a change
    if (error !== prevProps.error) {
      // Chek for register error
      if (error.id === LOGIN_FAIL) {
        this.setState({ msg: error.msg.msg });
      } else {
        this.setState({ msg: null });
      }
    }

    // If get authenticated close modal
    if (this.state.modal && isAuthenticated) {
      this.toggle();
    }
  }

  toggle = () => {
    // Clear errors
    this.props.clearErrors();

    this.setState({
      modal: !this.state.modal
    });
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  onSubmit = (e) => {
    e.preventDefault();

    const { email, password } = this.state;
    const user = { email, password };

    // Attempt to login
    this.props.login(user);
  }

  render() {
    return (
      <div>
        <NavLink onClick={this.toggle} href="#">
          Login
        </NavLink>

        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>Login</ModalHeader>
          <ModalBody>
            {this.state.msg && <Alert color="danger">{this.state.msg}</Alert>}
            <LoginForm onSubmit={this.onSubmit} onChange={this.onChange} />
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

LoginModal.propTypes = {
  isAuthenticaded: PropTypes.bool,
  error: PropTypes.object.isRequired,
  login: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.error
});

export default connect(mapStateToProps, { login, clearErrors })(LoginModal);
