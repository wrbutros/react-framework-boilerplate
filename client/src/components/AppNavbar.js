import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Container,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

import LoginModal from './auth/LoginModal';
import RegisterModal from './auth/RegisterModal';
import Logout from './auth/Logout';

class AppNavbar extends Component {
  state = {
    isOpen: false
  };

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  navLinks = () => {
    const { sections } = this.props;
    return Object.keys(sections).map((key, idx, arr) =>
      <Fragment key={`section_${key}`}>
        {
          sections[key].map((l, k) => (
            <DropdownItem key={`dd_item_${k}`} href={`${l.href}`}>
              {l.name}
            </DropdownItem>
          ))
        }
        {arr.length - 1 !== idx && <DropdownItem key={`separator_${key}`} divider />}
      </Fragment>
    )
  }

  authLinks = () => {
    const { user } = this.props.auth;
    return (
      <>
        {/* User Name */}
        <NavItem>
          <span className="navbar-text mr-3">
            <strong>{user && `Bienvenido ${user.name}`}</strong>
          </span>
        </NavItem>
        {/* Logout Button */}
        <NavItem>
          <Logout />
        </NavItem>
      </>
    )
  }

  guestLinks = () => (
    <>
      {/* SignUp Button */}
      <NavItem>
        <RegisterModal />
      </NavItem>
      {/* Login Button */}
      <NavItem>
        <LoginModal />
      </NavItem>
    </>
  )

  render() {
    const { auth: { isAuthenticated, user } } = this.props;
    return (
      <div>
        {/* expand: sm (small), md (medium), lg (large) screens */}
        {/* className: mb5 (margin bottom 5) move everything down under the navbar */}
        <Navbar color="dark" dark expand="sm" className="mb-5">
          {/* Container places the content in the middle */}
          <Container>
            {/* The Navbar component name */}
            <NavbarBrand href="/">Transbaluso</NavbarBrand>
            {/* A toggler for the Navbar */}
            <NavbarToggler onClick={this.toggle} className="mr-2" />

            {isAuthenticated && user.isAdmin &&
              <Collapse isOpen={this.state.isOpen} navbar>
                <Nav className="ml-auto" navbar>
                  <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                      Menu
                  </DropdownToggle>
                    <DropdownMenu right>
                      {this.navLinks()}
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </Nav>
              </Collapse>
            }

            {/* Add the elements in the Collapse component */}
            <Collapse isOpen={this.state.isOpen} navbar>
              {/* ml-auto: Margin Left Auto which will place the content at the right */}
              <Nav className="ml-auto" navbar>
                {isAuthenticated ? this.authLinks() : this.guestLinks()}
              </Nav>
            </Collapse>
          </Container>
        </Navbar>
      </div>
    );
  }
}

AppNavbar.propTypes = {
  auth: PropTypes.object.isRequired,
  sections: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth
});

export default connect(mapStateToProps)(AppNavbar);
