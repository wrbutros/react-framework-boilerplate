import { combineReducers } from 'redux';
import entityReducerGenerator from './entityReducerGenerator';
import authReducer from './authReducer';
import errorReducer from './errorReducer';

const usuarioReducer = entityReducerGenerator('usuario');
const tipoUsuarioReducer = entityReducerGenerator('tipoUsuario');

export default combineReducers({
  tipoUsuario: tipoUsuarioReducer,
  usuario: usuarioReducer,
  auth: authReducer,
  error: errorReducer
});
