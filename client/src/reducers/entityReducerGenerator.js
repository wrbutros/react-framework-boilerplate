import { GET_ERRORS, GET_ENTITY, ADD_ENTITY, SELECTED_ENTITY, DELETE_ENTITY, LOADING_ENTITY } from '../actions/types';

const initialState = {
  loading: false,
  list: [],
  selected: []
};

export default (entityName) => {
  // Entity Name for the case
  const entityUpperName = entityName.toUpperCase();

  return (state = initialState, action) => {
    switch (action.type) {
      case `${GET_ENTITY}${entityUpperName}S`:
        return {
          ...state,
          list: action.payload,
          loading: false
        };
      case `${SELECTED_ENTITY}${entityUpperName}`:
        return {
          ...state,
          selected: action.payload
        };
      case `${DELETE_ENTITY}${entityUpperName}`:
        // TODO: optimize this
        const entities = state.list.filter(entity => !action.payload.includes(entity._id));
        return {
          ...state,
          list: entities
        };
      case `${ADD_ENTITY}${entityUpperName}`:
        return {
          ...state,
          list: [...action.payload, ...state.list]
        };
      case `${LOADING_ENTITY}${entityUpperName}`:
        return {
          ...state,
          loading: true
        };
      case GET_ERRORS:
        return {
          ...state,
          loading: false
        };
      default:
        return state;
    }
  }
}
