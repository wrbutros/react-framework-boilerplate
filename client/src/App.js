import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

// Styles
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/AppHome.css';

// State store, Reducers and actions
import store from './store';
import { loadUser } from './actions/authActions';

// Components
import AppNavbar from './components/AppNavbar';
import {
  TipoUsuarioCRUD,
  UsuarioCRUD
} from './components/CRUDGenerator';

// Pages
import MainPage from './pages/MainPage';
import NotFoundPage from './pages/NotFoundPage';

const actionLinks = [{
  href: '/',
  name: 'Pagina Principal',
  component: MainPage
}];

const CRUDLinks = [{
  href: '/tipo-usuario',
  name: 'Tipo Usuario',
  component: TipoUsuarioCRUD
}, {
  href: '/usuario',
  name: 'Usuario',
  component: UsuarioCRUD
}];

const autoRoutes = () =>
  [...actionLinks, ...CRUDLinks].map(({ component, href }, k) =>
    <Route key={`main_menu_${k}`} exact path={href} component={component} />
  );

class App extends Component {
  componentDidMount = async () => {
    store.dispatch(loadUser());
  }

  render() {
    return (
      <Provider store={store}>
        <div className="App" >
          <AppNavbar {...{ sections: { actionLinks, CRUDLinks } }} />
          <Router>
            <Switch>
              {autoRoutes()}
              <Route path="/404" component={NotFoundPage} />
              <Redirect to="/404" />
            </Switch>
          </Router>
        </div>
      </Provider>
    );
  }
}

export default App;
