import axios from 'axios';
import {
  USER_LOADED,
  USER_LOADING,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT_SUCCESS,
  REGISTER_SUCCESS,
  REGISTER_FAIL
} from './types';
import { jsonHeaders, tokenConfig } from '../utils/header';
import { returnErrors } from './errorActions';

/**
 * LoadUser Action
 *
 * Check token & Load user
 */
export const loadUser = () => async (dispatch, getState) => {
  // User loading
  dispatch({ type: USER_LOADING });
  try {
    const { data: userWithToken } = await axios.get('/api/auth/user', tokenConfig(getState));
    dispatch({
      type: USER_LOADED,
      payload: userWithToken
    });
  } catch (err) {
    dispatch(returnErrors(err.response.data, err.response.status));
    dispatch({
      type: AUTH_ERROR,
    });
  }
};


/**
 * Register Action
 *
 * Register a new user
 */
export const register = ({ name, email, password }) => async (dispatch) => {
  // Headers
  const header = jsonHeaders();

  // Request body
  const body = JSON.stringify({ name, email, password });

  // Register
  try {
    const { data: newUser } = await axios.post('/api/users', body, header);
    dispatch({
      type: REGISTER_SUCCESS,
      payload: newUser
    })
  } catch (err) {
    dispatch(returnErrors(err.response.data, err.response.status, REGISTER_FAIL));
    dispatch({
      type: REGISTER_FAIL
    });
  }
}

/**
 * Login Action
 *
 * Login user
 */
export const login = ({ email, password }) => async (dispatch) => {
  // Headers
  const header = jsonHeaders();

  // Request body
  const body = JSON.stringify({ email, password });

  // Register
  try {
    const { data: authenticatedUser } = await axios.post('/api/auth', body, header);
    dispatch({
      type: LOGIN_SUCCESS,
      payload: authenticatedUser
    })
  } catch (err) {
    dispatch(returnErrors(err.response.data, err.response.status, LOGIN_FAIL));
    dispatch({
      type: LOGIN_FAIL
    });
  }
}


/**
 * Logout Action
 *
 * Logout a User
 */
export const logout = () => {
  return {
    type: LOGOUT_SUCCESS
  };
};
