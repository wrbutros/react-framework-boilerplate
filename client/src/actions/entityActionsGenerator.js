import axios from 'axios';
import {
  GET_ENTITY,
  ADD_ENTITY,
  SELECTED_ENTITY,
  DELETE_ENTITY,
  LOADING_ENTITY,
  ADD_FAIL_ENTITY,
  DEL_FAIL_ENTITY
} from './types';
import { returnErrors } from './errorActions';
import { tokenConfig } from '../utils/header';
import { objectToQueryString, loadQuery } from '../utils/utils';

export const getEntitiesAction = (entityName, endpoint, { GETPath = '', query }) => () => async (dispatch, getState) => {
  // Loading ....
  dispatch(setEntityLoading(entityName));

  const entityUpperName = entityName.toUpperCase();
  try {
    const localQuery = await loadQuery(query);
    const queryUrl = objectToQueryString(localQuery);
    const res = await axios.get(`/api/${endpoint}/${GETPath}${queryUrl}`, tokenConfig(getState));
    dispatch({
      type: `${GET_ENTITY}${entityUpperName}S`,
      payload: res.data
    });
  } catch (err) {
    dispatch(returnErrors(err.response.data, err.response.status));
  }
}

export const setSelectedEntityAction = (entityName) => (entities) => async (dispatch) => {
  const entityUpperName = entityName.toUpperCase();
  try {
    dispatch({
      type: `${SELECTED_ENTITY}${entityUpperName}`,
      payload: entities
    });
  } catch (err) {
    dispatch(returnErrors(err.response.data, err.response.status, `${SELECTED_ENTITY}${entityUpperName}`));
  }
}

export const deleteEntityAction = (entityName, endpoint) => (id) => async (dispatch, getState) => {
  const entityUpperName = entityName.toUpperCase();
  try {
    const ids = Array.isArray(id) ? id : [id];
    await axios.patch(`/api/${endpoint}`, { action: 'delete', ids }, tokenConfig(getState));
    dispatch({
      type: `${DELETE_ENTITY}${entityUpperName}`,
      payload: ids
    });
  } catch (err) {
    dispatch(returnErrors(err.response.data, err.response.status, `${DEL_FAIL_ENTITY}${entityUpperName}`));
  }
}

export const addEntityAction = (entityName, endpoint) => (entity) => async (dispatch, getState) => {
  const entities = Array.isArray(entity) ? entity : [entity];
  const entityUpperName = entityName.toUpperCase();

  try {
    const res = await axios.post(`/api/${endpoint}`, entities, tokenConfig(getState));
    dispatch({
      type: `${ADD_ENTITY}${entityUpperName}`,
      payload: res.data
    })
    return res;
  } catch (err) {
    dispatch(returnErrors(err.response.data, err.response.status, `${ADD_FAIL_ENTITY}${entityUpperName}`));
    return null;
  }
}

export const setEntityLoading = (entityName) => {
  const entityUpperName = entityName.toUpperCase();
  return {
    type: `${LOADING_ENTITY}${entityUpperName}`
  }
}
