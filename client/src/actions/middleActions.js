import axios from 'axios';

import { ADD_ENTITY, DELETE_ENTITY } from './types';
import { setEntityLoading, getEntitiesAction } from './entityActionsGenerator';
import { returnErrors } from './errorActions';
import { tokenConfig } from '../utils/header';

import Q from '../entities/queries';
import * as E from '../entities';
import * as S from '../entities/specials';

// REFRESHERS
// const refreshSolicitudesNoIngresadas = async (dispatch, getState) =>
//   await getEntitiesAction(
//     E.SolicitudEntity.name, E.SolicitudEntity.endpoint, { query: Q.solicitudesNoIngresadas }
//   )()(dispatch, getState);
  // await getEntitiesAction(
  //   S.Delivery.name, S.Delivery.endpoint, { GETPath: 'usuario/ordenes', query: Q.misOrdenesPendientes }
  // )()(dispatch, getState);


// ADD ORDEN ENVIO
// export const ingresarOrdenEnvios = (solicitudes) => async (dispatch, getState) => {
//   // Loading......
//   dispatch(setEntityLoading('SOLICITUD'));
//   try {
//     const res = await axios.post(
//       `/api/ordenes/ingresar`,
//       solicitudes,
//       tokenConfig(getState)
//     );

//     dispatch({
//       type: `${ADD_ENTITY}ORDEN`,
//       payload: res.data
//     });

//     await refreshSolicitudesNoIngresadas(dispatch, getState);

//   } catch (err) {
//     window.alert(`An error happened ${err.response.data.msg}`);
//     dispatch(returnErrors(err.response.data, err.response.status));
//   }
// }

