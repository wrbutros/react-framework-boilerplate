import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// Styles
import 'bootstrap/dist/css/bootstrap.min.css';

// TODO: Add style
class NotFoundPage extends Component {
  render() {
    return (
      <div>
        <h3>404 Page Not Found</h3>
        <p>
          Go back to{' '}
          <Link className="bold" to="/">
            Home
          </Link>
        </p>
      </div>
    );
  }
}

export default NotFoundPage;
