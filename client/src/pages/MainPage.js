import React, { Component } from 'react';

class MainPage extends Component {
  render() {
    return (
      <div style={{ marginInline: '2em' }}>
        <h1>MAIN PAGE</h1>
      </div>
    );
  }
}

export default MainPage;
