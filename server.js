import { require } from './require.js';

const express = require('express');
const mongoose = require('mongoose');
const config = require('config');
const path = require('path');

// Utils (DB initializer)
import { initializeCollections } from './utils/seeder.js';

// Routes
import tipoUsuarios from './routes/api/tipoUsuarios.js';
import usuarios from './routes/api/usuarios.js';
// User
import users from './routes/api/users.js';
import auth from './routes/api/auth.js';

//Initialize express into a variable
const app = express();
// Express gonna parser the body instead bodyparser
app.use(express.json({ limit: '60mb' }));
app.use(express.urlencoded({ extended: true, limit: '60mb' }));

// DB URI Config
const db = process.env.MONGO_URI ? process.env.MONGO_URI : config.get('mongoURI');

//Connect to mongo
mongoose
  .connect(db, {
    useNewUrlParser: true,
    useCreateIndex: true,
    //autoIndex: true,
    useUnifiedTopology: true
  })
  .then(() => console.log('MongoDB Connected...'))
  .catch(err => console.log(err));

// Use Routes
app.use('/api/tipoUsuarios', tipoUsuarios);
app.use('/api/usuarios', usuarios);
// User
app.use('/api/users', users);
app.use('/api/auth', auth);


// Serve static assets if in production
if (process.env.NODE_ENV === 'production') {
  // Set static folder
  app.use(express.static('client/build'));
  app.get('*', (_, res) => {
    // TODO: Check why '__dirname' doesn't works in Heroku. Using path.resolve('./') instead.
    res.sendFile(path.resolve(path.resolve("./"), 'client', 'build', 'index.html'));
  });
}

// Config port to run the server
// We do the deploy in Heroku, we gonna access to the PORT environment variable
const port = process.env.PORT || 5000;

// We want the app listen in this port by using
(async () => {
  // Initialize the DB just if it has not the required records
  await initializeCollections();
  app.listen(port, () => console.log(`Server started at por ${port}`));
})();
