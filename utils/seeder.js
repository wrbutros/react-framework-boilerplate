import * as fs from 'fs';
import { asyncFilter } from './array.js';

import User, { TipoUsuario } from '../models/User.js';

const initialsFilePath = 'utils/initials.json';
const additionalsFilePath = 'utils/additionals.json';

const initialDocuments = [{
  model: TipoUsuario,
  field: 'tipoUsuario'
}];

const additionalDocuments = [{
  model: User,
  field: 'user'
}];

const JSON_REFERENCE_FIELD = 'db_reference';

const detectDocumentForeignField = (obj) => {
  return Object.keys(obj).find(k => typeof obj[k] === 'object' &&
    obj[k].hasOwnProperty(JSON_REFERENCE_FIELD))
}

const getInitialDocument = (field) =>
  initialDocuments.find(d => d.field === field);

const saveInitialsFn = (dataFromFile) => async ({ model, field }) => {
  await model.insertMany(dataFromFile[field]);
}

const saveAdditionalsFn = (dataFromFile) => async ({ model, field }) => {
  const collectionToSave = dataFromFile[field];
  const dataToSave = await Promise.all(
    collectionToSave.map(async doc => {
      const foreignField = detectDocumentForeignField(doc);
      if (!foreignField)
        return doc;
      const Model = getInitialDocument(doc[foreignField][JSON_REFERENCE_FIELD].field).model;
      const refModel = await Model.findOne(doc[foreignField][JSON_REFERENCE_FIELD].query);
      return {...doc, tipo: refModel._id};
    })
  );
  await model.insertMany(dataToSave);
}


const insertDataFromFile = async (filePath, documents, insertFn) => {
  const documentsToInitialize = await
    asyncFilter(documents, async ({ model }) =>
      (await model.find()).length === 0
    );

  fs.readFile(filePath, 'utf8', async (err, fileData) => {
    if (err) throw err;

    const dataFromFile = JSON.parse(fileData);
    await Promise.all(
      documentsToInitialize.map(insertFn(dataFromFile))
    )
  });
}

export const initializeCollections = async () => {
  try {
    await insertDataFromFile(initialsFilePath, initialDocuments, saveInitialsFn);
    await insertDataFromFile(additionalsFilePath, additionalDocuments, saveAdditionalsFn);
  } catch (error) {
    console.error(error);
  }
};
