import { require } from '../require.js';
const mongoose = require('mongoose');

const subObjTest = (e, filter, eq) =>
  !Object.keys(filter).map(k =>
    Array.isArray(e[k]) ?
      //Explore Array
      (e[k].length > 0 ? multiFilter(e[k], filter[k], eq).length > 0 : false) :
      // Explore Object
      e[k] != null && typeof e[k] === 'object' && !(e[k] instanceof mongoose.Types.ObjectId) ?
        subObjTest(e[k], filter[k], eq) :
        eq(`${e[k]}`, `${filter[k]}`)
  ).some(r => !r);

export const multiFilter = (obj, filter, eq = (a, b) => a === b) => {
  if (!filter) return obj;
  if (!obj || (Array.isArray(obj) && !obj.length)) return null;

  return obj.filter(e => subObjTest(e, filter, eq));
}

export const asyncFilter = async (arr, callback) =>
  (await Promise.all(
    arr.map(async item => (await callback(item)) ? item : null)
  )).filter(i => i);

// SUB-FIELD SELECTOR
export const fieldSelector = (pathField) => (obj) => {
  if (!obj) return 'NOT FOUND';
  if (typeof obj === 'string') return obj;
  const path = pathField.split('.');
  return path.reduce((currField, key) => currField ? (currField[key] || 'NOT FOUND') : 'NOT FOUND', obj);
}


const subFieldFilter = (a, b, field) => {
  if (fieldSelector(field.name)(a) < fieldSelector(field.name)(b)) {
    return field.asc ? -1 : 1;
  }
  if (fieldSelector(field.name)(a) > fieldSelector(field.name)(b)) {
    return field.asc ? 1 : -1;
  }
  return 0;
}

// array: array to filter
// sortFields: {name, asc}
export const multisort = (array, sortFields) =>
  sortFields.reduce((sorted, field, idx, fields) =>
    (idx === 0) ?
      sorted.sort((a, b) => subFieldFilter(a, b, field)) :
      sorted.sort((a, b) =>
        (fieldSelector(fields[idx - 1].name)(a) ===
         fieldSelector(fields[idx - 1].name)(b)) ?
          subFieldFilter(a, b, field) :
          0)
    , array)
